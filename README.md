<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->
![pipeline](https://img.shields.io/gitlab/pipeline-status/onsive.net/flle?style=for-the-badge)![release](https://img.shields.io/gitlab/v/tag/onsive.net/flle?label=release&sort=semver&style=for-the-badge)![pre release](https://img.shields.io/gitlab/v/tag/onsive.net/flle?include_prereleases&label=pre%20release&sort=semver&style=for-the-badge)
<br /><br />

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!-- <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

  <h3 align="center">FLLE</h3>

  <p align="center">
    <b>F</b>lashing<b>L</b>ights <b>L</b>ight <b>E</b>ditor
    <!-- <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a> -->
    <br />
    <br />
    <a href="https://gitlab.com/onsive.net/flle/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/onsive.net/flle/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

The ingame lights editor of FlashingLights is somewhat unusable at the moment. That's why I started this project.

It allows easier editing of the light sequences of emergency vehicles.

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Material Design In XAML](http://materialdesigninxaml.net/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

### Installation

1. Download the latest release zip file ([Releases](https://gitlab.com/onsive.net/flle/-/releases))
2. Unzip the zip file
3. Run the `FlashingLights.LightEditor.exe`

> If the application won't start, make sure you have the .NET Framework 4.5 installed ([Microsoft download page](https://www.microsoft.com/en-us/download/details.aspx?id=30653))

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

1. Go ingame in the vehicle editor
2. Select your vehicle
3. Click on `EDIT LIGHTS`
4. Select the light you want to change
5. Click on the small pen on the top right of that light
6. Click `Export Lightning Pattern` on the top right
7. Save it somewhere
8. Drag & Drop the exported lightning pattern in FLLE
9. Make your changes
10. Click on `save` in FLLE
11. Click on `Import Lightning Pattern` in FL
12. Select the in 10. chosen file
13. Click on `SAVE & EXIT` on the top
14. Repeat until you're satisfied

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [X] Easy lighting editor [1.0.0-alpha+1]
- [X] Make UI material design [1.0.0-alpha+5]
- [x] Support timing
- [ ] Support cycling lights
- [ ] Support glass color
- [ ] Preview lighting sequence

See the [open issues](https://gitlab.com/onsive.net/flle/-/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



## Contact

Hille - [Hille#0001](https://discordapp.com/users/235779852874153994) - hille@onsive.net

Project Link: [https://gitlab.com/onsive.net/flle](https://gitlab.com/onsive.net/flle)

<p align="right">(<a href="#top">back to top</a>)</p>



## Acknowledgments

* [Best-README-Template](https://github.com/othneildrew/Best-README-Template)

<p align="right">(<a href="#top">back to top</a>)</p>
