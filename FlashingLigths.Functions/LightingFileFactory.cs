﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace FlashingLights.Serializer
{
    public static class LightingFileFactory
    {
        public static Lights Configuration { get; internal set; } = null;

        public static LightingFile Serialize(string path)
        {
            if (Configuration == null)
            {
                var mySerializer = new XmlSerializer(typeof(Lights));
                using (var myFileStream = new FileStream("Config/Lights.xml", FileMode.Open))
                    Configuration = (Lights)mySerializer.Deserialize(myFileStream);
            }

            var fileLines = File.ReadAllLines(path);
            var lightModelName = fileLines[3];
            var lightSequence = fileLines[4].Split('&');
            var colors = lightSequence[0].Split('*');
            var durations = lightSequence[1].Split('?');

            var light = Configuration.Light.FirstOrDefault(x => x.Name == lightModelName);
            if (light == default)
                throw new NotImplementedException("The passed light file is not implemented yet or missformatted!");

            string[,] colorMatrix = new string[colors.Length, colors[0].Count(x => x == '?') + 1];

            var steps = new List<Step>();

            for (int i = 0; i < colors.Length; i++)
            {
                var l = colors[i].Split('?') ?? new string[0];
                for (int j = 0; j < l.Length; j++)
                    try
                    {
                        colorMatrix[i, j] = l[j];
                    }
                    catch
                    {
                        colorMatrix[i, j] = "";
                    }
            }

            var glassColors = new List<Color>();
            for (int i = 0; i < colorMatrix.GetLength(1); i++)
            {
                var list = new List<Color>();
                for (int j = 0; j < colorMatrix.GetLength(0); j++)
                    if (light.GlassColor > 0 && j == colorMatrix.GetLength(0) - 1)
                        glassColors.Add((Color)int.Parse(colorMatrix[j, i]));
                    else
                        list.Add((Color)int.Parse(colorMatrix[j, i]));

                steps.Add(new Step
                {
                    Duration = double.Parse(durations[i], NumberStyles.Number, CultureInfo.InvariantCulture),
                    Lights = list.ToArray()
                });
            }

            return new LightingFile()
            {
                GlassColors = glassColors.ToArray(),
                LightModelName = lightModelName,
                Steps = steps,
                OriginalSequence = fileLines[4],
                Light = light,
                SavePath = path
            };
        }
    }
}
