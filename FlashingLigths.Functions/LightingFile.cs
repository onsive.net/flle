﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FlashingLights.Serializer
{
    public class LightingFile
    {
        public Light Light { get; set; }
        public string LightModelName { get; set; }
        public List<Step> Steps { get; set; } = new List<Step>();
        internal string OriginalSequence { get; set; }
        public Color[] GlassColors { get; set; } = null;
        public string SavePath { get; set; } = null;

        internal LightingFile()
        {
        }

        public void Save()
            => WriteTo(SavePath);

        public void WriteTo(string path)
        {
            var sequence = OriginalSequence.Split('&');
            sequence[0] = "";
            sequence[1] = "";

            var steps = Steps.Where(x => x != default).ToList();
            for (int i = 0; i < steps[0].Lights.Count(); i++)
            {
                foreach (var step in steps)
                    sequence[0] += (int)step.Lights[i] + "?";
                sequence[0] = CutOfLastChar(sequence[0]) + "*";
            }

            foreach (var step in steps)
                sequence[1] += step.Duration.ToString("0.00", CultureInfo.InvariantCulture) + "?";

            sequence[0] = CutOfLastChar(sequence[0]);
            sequence[1] = CutOfLastChar(sequence[1]);

            var fileContent = new List<string>
            {
                "Flashing Lights game file for emergency lights setup.",
                "",
                "Light model name:",
                LightModelName,
                string.Join("&", sequence),
                "",
                "* Adjusting this file manually may break the import/export functionality!",
            };
            Functions.WriteAllLines(path, fileContent, "\n");
        }

        private string CutOfLastChar(string s)
            => s.Substring(0, s.Length - 1);
    }
}