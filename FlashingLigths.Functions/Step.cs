﻿namespace FlashingLights.Serializer
{
    public class Step
    {
        public double Duration { get; set; }
        public Color[] Lights { get; set; }
    }
}