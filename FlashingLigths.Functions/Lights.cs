﻿using System;

namespace FlashingLights.Serializer
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(Namespace = "", IsNullable = false)]
    public partial class Lights
    {

        private Light[] lightField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("Light")]
        public Light[] Light
        {
            get
            {
                return lightField;
            }
            set
            {
                lightField = value;
            }
        }
    }

    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class Light
    {

        private LightRow[] rowField;

        private string nameField;

        private int glassColorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("Row")]
        public LightRow[] Row
        {
            get
            {
                return rowField;
            }
            set
            {
                rowField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttribute()]
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttribute()]
        public int GlassColor
        {
            get
            {
                return glassColorField;
            }
            set
            {
                glassColorField = value;
            }
        }
    }

    /// <remarks/>
    [Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public partial class LightRow
    {
        private bool invertField = false;

        private int indexField = 0;

        private int lightsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttribute()]
        public bool Invert
        {
            get
            {
                return invertField;
            }
            set
            {
                invertField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttribute()]
        public int Index
        {
            get
            {
                return indexField;
            }
            set
            {
                indexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttribute()]
        public int Lights
        {
            get
            {
                return lightsField;
            }
            set
            {
                lightsField = value;
            }
        }
    }
}
