﻿namespace FlashingLights.Serializer
{
    public enum Color
    {
        None = 0,
        Red = 1,
        Blue = 2,
        Green = 3,
        Orange = 4,
        White = 5,
    }
}