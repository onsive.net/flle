﻿using System;
using FlashingLights.LightEditor.Models;
using System.IO;
using System.Windows;
using System.Xml.Serialization;
using Backtrace.Model;

namespace FlashingLights.LightEditor
{
    internal static class Functions
    {
        internal static void SaveConfiguration()
        {
            try
            {
                XmlSerializer writer = new XmlSerializer(typeof(AppConfig));
                using (FileStream file = File.Create(Paths.ConfigFile))
                    writer.Serialize(file, Variables.Configuration);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        internal static void HandleError(Exception exception)
            => HandleError(new BacktraceReport(exception));

        internal static void HandleError(BacktraceReport backtraceReport)
        {
#if DEBUG
            Console.WriteLine("Error: " + backtraceReport.Exception.Message);
            Console.WriteLine(backtraceReport.Exception.StackTrace);
#endif
            backtraceReport.Attributes.Add("application.version", Variables.Version);
            Variables.BacktraceClient?.Send(backtraceReport);

            MessageBox.Show(
                "An error occured! If the error persists, please file a bug report at https://gitlab.com/onsive.net/flle/-/issues with the steps to reproduce the error.",
                "FLLE | Exception",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }
    }
}