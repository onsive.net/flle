﻿using System;
using System.Collections.Generic;

namespace FlashingLights.LightEditor.Models
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("FLLE", Namespace = "", IsNullable = false)]
    public partial class AppConfig
    {
        private AppConfigLauncher launcherField = new AppConfigLauncher();

        private AppConfigEditor editorField = new AppConfigEditor();

        private string versionField = null;

        private int errorReportingField = -1;

        /// <remarks/>
        public AppConfigLauncher Launcher
        {
            get { return this.launcherField; }
            set { this.launcherField = value; }
        }

        /// <remarks/>
        public AppConfigEditor Editor
        {
            get { return this.editorField; }
            set { this.editorField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version
        {
            get { return this.versionField; }
            set { this.versionField = value ?? Variables.Version; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int ErrorReporting
        {
            get { return this.errorReportingField; }
            set { this.errorReportingField = value; }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AppConfigLauncher
    {
        private List<AppConfigLightningFile> recentOpenedLightingFilesField = new List<AppConfigLightningFile>();

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("LightningFile", IsNullable = false)]
        public List<AppConfigLightningFile> RecentOpenedLightingFiles
        {
            get { return this.recentOpenedLightingFilesField; }
            set { this.recentOpenedLightingFilesField = value; }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AppConfigLightningFile
    {
        private DateTime dateField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public DateTime Date
        {
            get { return this.dateField; }
            set { this.dateField = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get { return this.valueField; }
            set { this.valueField = value; }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AppConfigEditor
    {
        private string lastSavePathField = null;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string LastSavePath
        {
            get { return this.lastSavePathField; }
            set { this.lastSavePathField = value; }
        }
    }
}