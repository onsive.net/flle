﻿using FlashingLights.LightEditor.Windows;
using System;
using System.IO;
using System.Linq.Expressions;
using System.Windows;
using System.Xml.Serialization;
using Backtrace;
using Backtrace.Model;
using FlashingLights.LightEditor.Models;

namespace FlashingLights.LightEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string path = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData))
                .FullName;
            if (Environment.OSVersion.Version.Major >= 6)
            {
                path = Directory.GetParent(path).ToString();
            }

            Paths.UserDirectory = Path.Combine(path, "FLLE");
            if (!Directory.Exists(Paths.UserDirectory))
                Directory.CreateDirectory(Paths.UserDirectory);

            try
            {
                var mySerializer = new XmlSerializer(typeof(AppConfig));
                using (var myFileStream = new FileStream(Paths.ConfigFile, FileMode.Open))
                    Variables.Configuration = (AppConfig) mySerializer.Deserialize(myFileStream);
            }
            catch
            {
                Variables.Configuration = new AppConfig();
            }

            if (Variables.Configuration.ErrorReporting < 0)
            {
                var msgBox = new CustomMessageBox(new MessageBoxArgs()
                {
                    Title = "Send Error Reports",
                    Text = @"Would you like to participate in sending error reports to the Backtrace server?

There will be no system information send, just the plain error.
It would really help us improve the application as we then know in which part of the application an error could be thrown.
To change this settings go to your user folder/FLLE/config.xml and change ErrorReporting to -1 and restart the application.",
                    Button1Txt = "No thanks",
                    Button2Txt = "I agree"
                });
                msgBox.ShowDialog();
                switch (msgBox.Result)
                {
                    case 1:
                        Variables.Configuration.ErrorReporting = 0;
                        break;
                    case 2:
                        Variables.Configuration.ErrorReporting = 1;
                        break;
                    default:
                        Variables.Configuration.ErrorReporting = -1;
                        break;
                }
                Functions.SaveConfiguration();
            }

            if (Variables.Configuration.ErrorReporting == 1)
            {
                var credentials = new BacktraceCredentials(
                    @"https://submit.backtrace.io/onsive/71b069737f068e604d049f8f1a7918df193e69168d01fab6a4fa509988295d3d/json",
                    "71b069737f068e604d049f8f1a7918df193e69168d01fab6a4fa509988295d3d"
                );
                Variables.BacktraceClient = new BacktraceClient(credentials);
                Variables.BacktraceClient.HandleApplicationException();
            }
            else
                Variables.BacktraceClient = null;

            Current.MainWindow = new Launcher();
            Current.MainWindow.Show();
        }
    }
}