﻿using FlashingLights.LightEditor.Windows;
using FlashingLights.Serializer;
using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;

namespace FlashingLights.LightEditor.Components
{
    /// <summary>
    /// Interaction logic for MenuComponent.xaml
    /// </summary>
    public partial class MenuComponent : UserControl
    {
        public static readonly DependencyProperty LightingFileProperty = DependencyProperty.Register("LightingFile", typeof(LightingFile), typeof(MenuComponent), new FrameworkPropertyMetadata(null));
        public LightingFile LightingFile
        {
            get
            {
                return (LightingFile)GetValue(LightingFileProperty);
            }
            set
            {
                SetValue(LightingFileProperty, value);
            }
        }

        public MenuComponent()
        {
            InitializeComponent();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
            => Save(false);

        private void SaveAsClick(object sender, RoutedEventArgs e)
            => Save(true);

        private void Save(bool showPathDialog)
        {
            try
            {
            if (showPathDialog || string.IsNullOrWhiteSpace(LightingFile.SavePath))
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog()
                {
                    FileName = Variables.Configuration.Editor?.LastSavePath,
                    Title = "Save Lighting Files",

                    DefaultExt = "txt",
                    Filter = "Lighting Files (*.txt)|*.txt",
                    FilterIndex = 2,
                    RestoreDirectory = true
                };

                if (saveFileDialog.ShowDialog() == true)
                    LightingFile.SavePath = saveFileDialog.FileName;
                else
                    return;
            }
            LightingFile.Save();
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Functions.SaveConfiguration();
                ((Editor) ((FrameworkElement) Parent).Parent).Close();
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }
    }
}
