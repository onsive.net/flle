﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace FlashingLights.LightEditor.Components
{
    /// <summary>
    /// Interaction logic for LigthControl.xaml
    /// </summary>
    public partial class LightControl : Shape
    {
        private RectangleGeometry rect = new RectangleGeometry(new Rect(0, 0, 35, 20));

        public LightControl()
        {
            InitializeComponent();
        }

        protected override Geometry DefiningGeometry
        {
            get
            {
                return rect;
            }
        }
    }
}
