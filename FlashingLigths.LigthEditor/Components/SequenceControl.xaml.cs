﻿using System;
using FlashingLights.LightEditor.Windows;
using FlashingLights.Serializer;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace FlashingLights.LightEditor.Components
{
    /// <summary>
    /// Interaction logic for SequenceObject.xaml
    /// </summary>
    public partial class SequenceControl : UserControl
    {
        private Editor editor { get; set; }

        public string title { get; set; }
        public double duration { get; set; }
        public Step step { get; set; }

        public SequenceControl(Editor editor, int seuqenceId, Step step)
        {
            try
            {
                this.editor = editor;
                this.DataContext = step;
                title = "Step " + seuqenceId;
                InitializeComponent();
                this.duration = step.Duration;
                this.step = step;
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                editor.ChangeStep(step);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void OnDeleteClick(object sender, RoutedEventArgs e)
        {
            try
            {
                editor.RemoveStep(this, step);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        public void SetStatus(bool isActive)
        {
            try
            {
                if (isActive)
                    backgroundBorder.Background = Brushes.LightGray;
                else
                    backgroundBorder.Background = null;
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }
    }
}