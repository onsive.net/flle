﻿using FlashingLights.LightEditor.Models;
using FlashingLights.Serializer;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;
using Backtrace.Model;

namespace FlashingLights.LightEditor.Windows
{
    /// <summary>
    /// Interaction logic for Launcher.xaml
    /// </summary>
    public partial class Launcher : Window
    {
        internal readonly List<ListBoxItem> RecentOpenedLightingFiles = new List<ListBoxItem>();

        public Launcher()
        {
            try
            {
                InitializeComponent();

                Title = $"[{Variables.Version}] {Variables.Name}";

                PopulateLatestOpenedFiles();
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void PopulateLatestOpenedFiles()
        {
            try
            {
                RecentOpenedLightingFiles.Clear();
                if (Variables.Configuration.Launcher?.RecentOpenedLightingFiles == null)
                    return;

                var list = Variables.Configuration.Launcher.RecentOpenedLightingFiles.OrderByDescending(x => x.Date)
                    .ToArray();
                for (int i = 0; i < list.Length && i < 10; i++)
                {
                    string content;
                    if (list[i].Value.Length > 35)
                        content = "..." + list[i].Value.Substring(list[i].Value.Length - 35);
                    else
                        content = list[i].Value;
                    RecentOpenedLightingFiles.Add(new ListBoxItem()
                    {
                        Content = content,
                        Tag = list[i]
                    });
                }

                recentOpenedLightingFilesListBox.ItemsSource = RecentOpenedLightingFiles;
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void latestFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selectedRecentOpenedLightingFile = recentOpenedLightingFilesListBox.SelectedItem;

                var recentOpenedLightingFileItem = selectedRecentOpenedLightingFile as ListBoxItem;
                if (recentOpenedLightingFileItem == null)
                    return;

                var recentOpenedLightingFile = recentOpenedLightingFileItem.Tag as AppConfigLightningFile;
                if (recentOpenedLightingFile == null)
                    return;

                if (!File.Exists(recentOpenedLightingFile.Value))
                    RecentOpenedLightingFiles.Remove(recentOpenedLightingFileItem);

                OpenLightningFile(recentOpenedLightingFile.Value);

                recentOpenedLightingFilesListBox.SelectedIndex = -1;
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void OpenLightingFileClick(object sender, RoutedEventArgs e)
        {
            try
            {
                string initialDirectory = null;
                if (!string.IsNullOrEmpty(Variables.Configuration.Editor.LastSavePath))
                    initialDirectory = new FileInfo(Variables.Configuration.Editor.LastSavePath).DirectoryName;

                var openFileDialog = new OpenFileDialog
                {
                    InitialDirectory = initialDirectory,
                    Title = "Browse Lighting Files",

                    CheckFileExists = true,
                    CheckPathExists = true,

                    DefaultExt = "txt",
                    Filter = "Lighting Files (*.txt)|*.txt",
                    FilterIndex = 2,
                    RestoreDirectory = true,

                    ReadOnlyChecked = true,
                    ShowReadOnly = true
                };

                if (openFileDialog.ShowDialog() == true)
                {
                    OpenLightningFile(openFileDialog.FileName);

                    if (string.IsNullOrEmpty(Variables.Configuration.Editor.LastSavePath))
                        Variables.Configuration.Editor.LastSavePath = openFileDialog.FileName;
                }
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void OpenLightningFile(string path)
        {
            try
            {
                var fileInfo = new FileInfo(path);
                if (!fileInfo.Exists)
                    return;

                var lightingFile = LightingFileFactory.Serialize(fileInfo.FullName);

                var lastOpenedFile =
                    Variables.Configuration.Launcher.RecentOpenedLightingFiles.FirstOrDefault(x => x.Value == path);
                if (lastOpenedFile == default)
                {
                    lastOpenedFile = new AppConfigLightningFile()
                    {
                        Value = path
                    };
                    Variables.Configuration.Launcher.RecentOpenedLightingFiles.Add(lastOpenedFile);
                }

                lastOpenedFile.Date = DateTime.Now;

                var editor = new Editor(lightingFile);
                editor.Show();
                WindowState = WindowState.Minimized;

                PopulateLatestOpenedFiles();
                Functions.SaveConfiguration();
            }
            catch (Exception exception)
            {
                var backtraceReport = new BacktraceReport(
                    exception,
                    attachmentPaths: new List<string>() {path})
                {
                    Fingerprint = "058788dcd084790b1a2a1cf164f746ad24d54d298bb45b311137f285888a1e42",
                    Factor = exception.GetType().Name
                };
                Functions.HandleError(backtraceReport);
            }
        }

        private void Launcher_OnClosing(object sender, CancelEventArgs e)
        {
            try
            {
                if (Application.Current.Windows.Count == 1)
                    Environment.Exit(0);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }
    }
}