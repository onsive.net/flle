﻿using System;
using FlashingLights.LightEditor.Components;
using FlashingLights.Serializer;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace FlashingLights.LightEditor.Windows
{
    public partial class Editor : Window
    {
        public LightingFile LightingFile { get; set; }

        private Step currentStep;
        private readonly List<LightControl> lightControlls = new List<LightControl>();

        private Serializer.Color SelectedColor = Serializer.Color.None;

        private readonly Dictionary<Serializer.Color, Brush> Colors = new Dictionary<Serializer.Color, Brush>
        {
            {Serializer.Color.None, Brushes.LightGray},
            {Serializer.Color.Red, Brushes.Red},
            {Serializer.Color.Blue, Brushes.Blue},
            {Serializer.Color.Green, Brushes.LimeGreen},
            {Serializer.Color.Orange, Brushes.Orange},
            {Serializer.Color.White, Brushes.White},
        };

        public Editor(LightingFile lightingFile)
        {
            try
            {
                this.LightingFile = lightingFile;
                this.DataContext = this;

                InitializeComponent();

                Title = $"{Variables.ShortName} | {lightingFile.SavePath}";

                if (lightingFile.Steps?.Any() != true)
                    lightingFile.Steps = new List<Step>()
                    {
                        new Step
                        {
                            Duration = 0.05,
                            Lights = new Serializer.Color[lightingFile.Light.Row.Sum(x => x.Lights)]
                        }
                    };

                GenerateSequences();
                GenerateButtons();
                ChangeStep(lightingFile.Steps[0]);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        internal void RemoveStep(SequenceControl sequenceControl, Step step)
        {
            try
            {
                if (LightingFile.Steps.Count(x => x != null) == 1)
                    return;

                var index = LightingFile.Steps.IndexOf(step);
                LightingFile.Steps[index] = default;
                sequenceStackPanel.Children.Remove(sequenceControl);

                ChangeStep(((SequenceControl) sequenceStackPanel.Children[sequenceStackPanel.Children.Count - 1]).step);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        public void ChangeStep(Step step)
        {
            try
            {
                currentStep = step;
                for (int i = 0; i < step.Lights.Length; i++)
                {
                    lightControlls[i].Fill = Colors[step.Lights[i]];
                }

                foreach (SequenceControl item in sequenceStackPanel.Children)
                {
                    if (item.step == step)
                        item.SetStatus(true);
                    else
                        item.SetStatus(false);
                }
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void GenerateSequences()
        {
            try
            {
                sequenceStackPanel.Children.Clear();
                for (int i = 0; i < LightingFile.Steps.Count; i++)
                    sequenceStackPanel.Children.Add(new SequenceControl(this, i, LightingFile.Steps[i]));
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void GenerateButtons()
        {
            try
            {
                designStackPanel.Children.Clear();
                lightControlls.Clear();

                int counter = 0;

                var dict = new Dictionary<int, List<LightControl>>();

                for (int i = 0; i < LightingFile.Light.Row.Length; i++)
                {
                    if (!dict.ContainsKey(LightingFile.Light.Row[i].Index))
                        dict.Add(LightingFile.Light.Row[i].Index, new List<LightControl>());

                    if (LightingFile.Light.Row[i].Invert)
                    {
                        List<LightControl> tmp = new List<LightControl>();
                        for (int j = 0; j < LightingFile.Light.Row[i].Lights; j++)
                            tmp.Add(GenreateRectangle(counter++));
                        for (int j = tmp.Count; j-- > 0;)
                            dict[LightingFile.Light.Row[i].Index].Add(tmp[j]);
                    }
                    else
                        for (int j = 0; j < LightingFile.Light.Row[i].Lights; j++)
                            dict[LightingFile.Light.Row[i].Index].Add(GenreateRectangle(counter++));
                }

                foreach (var item in dict.OrderBy(x => x.Key))
                {
                    var stackPanel = new StackPanel();
                    stackPanel.Orientation = Orientation.Horizontal;

                    bool addSpacer = false;
                    if (item.Value.Count == 2 && dict[0].Count > 2)
                        addSpacer = true;

                    for (int i = 0; i < item.Value.Count; i++)
                    {
                        if (addSpacer && i == 1)
                            stackPanel.Children.Add(new Label()
                            {
                                Width = (dict[0].Count - 2) * 35.5
                            });
                        stackPanel.Children.Add(item.Value[i]);
                    }

                    designStackPanel.Children.Add(stackPanel);
                }
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private LightControl GenreateRectangle(int id)
        {
            try
            {
                var rectangle = new LightControl();
                rectangle.MouseLeftButtonDown += OnLightLeftClick;
                rectangle.MouseRightButtonDown += OnLightRigthClick;
                rectangle.MouseEnter += OnLightMouseOver;
                rectangle.Tag = id;
                lightControlls.Add(rectangle);
                return rectangle;
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
                throw;
            }
        }

        private void OnLightMouseOver(object sender, MouseEventArgs e)
        {
            try
            {
                var lightControl = sender as LightControl;
                if (lightControl == null)
                    return;

                if (e.LeftButton == MouseButtonState.Pressed)
                    ChangeColor(lightControl, SelectedColor);
                else if (e.RightButton == MouseButtonState.Pressed)
                    ChangeColor(lightControl, Serializer.Color.None);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void OnLightLeftClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var lightControl = sender as LightControl;
                if (lightControl == null)
                    return;

                ChangeColor(lightControl, SelectedColor);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void OnLightRigthClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var lightControl = sender as LightControl;
                if (lightControl == null)
                    return;

                ChangeColor(lightControl, Serializer.Color.None);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void ChangeColor(LightControl lightControl, Serializer.Color color)
        {
            try
            {
                lightControl.Fill = Colors[color];
                currentStep.Lights[(int) lightControl.Tag] = color;
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var btn = sender as Button;
                if (btn == null)
                    return;

                SelectColor((Serializer.Color) btn.Tag);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.Key)
                {
                    case Key.D1:
                        SelectColor((Serializer.Color) 1);
                        break;
                    case Key.D2:
                        SelectColor((Serializer.Color) 2);
                        break;
                    case Key.D3:
                        SelectColor((Serializer.Color) 3);
                        break;
                    case Key.D4:
                        SelectColor((Serializer.Color) 4);
                        break;
                    case Key.D5:
                        SelectColor((Serializer.Color) 5);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void SelectColor(Serializer.Color color)
        {
            try
            {
                SelectedColor = color;
                foreach (Button item in colorStackPanel.Children)
                {
                    if (item.Tag.Equals(color))
                        item.IsEnabled = false;
                    else
                        item.IsEnabled = true;
                }
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void AddStepClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var step = new Step()
                {
                    Duration = currentStep.Duration,
                    Lights = new Serializer.Color[currentStep.Lights.Length]
                };
                LightingFile.Steps.Add(step);
                sequenceStackPanel.Children.Add(new SequenceControl(this, LightingFile.Steps.Count(), step));
                ChangeStep(step);
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }

        private void Editor_OnClosing(object sender, CancelEventArgs e)
        {
            try
            {
                if (Application.Current.Windows.Count == 1)
                    Environment.Exit(0);
                else
                {
                    var launcher = Application.Current.Windows.OfType<Launcher>().FirstOrDefault();
                    if (launcher != null)
                    {
                        launcher.WindowState = WindowState.Normal;
                        launcher.Focus();
                    }
                }
            }
            catch (Exception exception)
            {
                Functions.HandleError(exception);
            }
        }
    }
}