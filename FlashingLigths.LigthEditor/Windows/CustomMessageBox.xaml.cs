using System.Windows;

namespace FlashingLights.LightEditor.Windows
{
    public class MessageBoxArgs
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string Button1Txt { get; set; }
        public string Button2Txt { get; set; }
    }

    public partial class CustomMessageBox : Window
    {
        public int Result { get; private set; } = 0;

        public CustomMessageBox(MessageBoxArgs args)
        {
            InitializeComponent();
            this.DataContext = args;
        }

        private void Button1Click(object sender, RoutedEventArgs e)
        {
            Result = 1;
            Close();
        }

        private void Button2Click(object sender, RoutedEventArgs e)
        {
            Result = 2;
            Close();
        }
    }
}