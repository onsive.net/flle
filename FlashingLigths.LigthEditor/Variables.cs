﻿using Backtrace;
using FlashingLights.LightEditor.Models;

namespace FlashingLights.LightEditor
{
    public static class Variables
    {
        public const string Name = "FlashingLights | Light Editor";
        public const string ShortName = "FLLE";
        public const string Version = "1.0.0-alpha+6";
        public const string Athor = "Hille";
        public const string Copyrigth = "2021 (c) OnSive.net";

        public static AppConfig Configuration;
        public static BacktraceClient BacktraceClient;
    }
}
