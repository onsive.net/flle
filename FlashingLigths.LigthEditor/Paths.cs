﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashingLights.LightEditor
{
    internal static class Paths
    {
        internal static string UserDirectory { get; set; }
        internal static string ConfigFile
            => Path.Combine(UserDirectory, "config.xml");
    }
}
